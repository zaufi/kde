# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# copyright 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] gtk-icon-cache

SUMMARY="Puzzle based on anagrams of words"
DESCRIPTION="
The puzzle is solved when the letters of the scrambled word are put back in the correct order.
There is no limit on either time taken, or the amount of attempts to solve the word. Included
with Kanagram are several vocabularies ready to play, with many more available from the Internet.
A scoring system has been added recently that can be customized from the settings."

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS+=" tts [[ description = [ Support for text to speech ] ]]"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5
    build+run:
        kde/libkeduvocdocument:${SLOT}
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/kcrash:5
        kde-frameworks/ki18n:5
        kde-frameworks/kio:5
        kde-frameworks/knewstuff:5
        kde-frameworks/sonnet:5
        x11-libs/qtbase:5[gui]
        x11-libs/qtdeclarative:5
        tts? ( x11-libs/qtspeech:5 )
    run:
        kde/kdeedu-data:${SLOT}
        x11-libs/qtmultimedia:5 [[ note = [ import QtMultimedia 5.0 ] ]]
        x11-libs/qtquickcontrols:5
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'tts Qt5TextToSpeech' )

