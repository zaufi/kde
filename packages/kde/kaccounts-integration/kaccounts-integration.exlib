# Copyright 2015-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ] test-dbus-daemon

SUMMARY="Small system to administer web accounts for sites and services"

LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS=""

KF5_MIN_VER=5.91.0
QT_MIN_VER=5.15.0

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        net-libs/accounts-qt[>=1.13][-qt4]
        net-libs/signon[>=8.55][-qt4]
        x11-libs/qtbase:5[>=5.7.0]
        x11-libs/qtdeclarative:5[>=5.7.0]
    run:
        dev-util/intltool [[ note = [ intltool-merge in kaccounts_add_* cmake macros ] ]]
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 kcm_kaccounts ] ]]
        x11-libs/qtquickcontrols:5[>=5.2.0]
    post:
        kde/kaccounts-providers:4 [[ note = [ Unbreak a cycle with this package ] ]]
    recommendation:
        kde/signon-kwallet-extension:4 [[
            description = [ Support for storing the login credentials of online accounts in KWallet ]
        ]]
        net-apps/signon-ui [[
            description = [ Required for the login process of some online accounts, e.g. Google ]
        ]]
        net-libs/signon-plugin-oauth2 [[
            description = [ Required for the login process of some online accounts, e.g. Google ]
        ]]
"

