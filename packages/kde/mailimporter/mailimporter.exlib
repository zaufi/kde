# Copyright 2016-2021 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Mail importer library"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

KF5_MIN_VER=5.95.0
QT_MIN_VER=5.15.2

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

mailimporter_src_test() {
    xdummy_start

    default

    xdummy_stop
}

