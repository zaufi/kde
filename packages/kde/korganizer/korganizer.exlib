# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="A KDE a calendar, todo-list manager and journal"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS="
    userfeedback [[
        description = [ Allows sending anonymized usage information to KDE ]
    ]]
"

KF5_MIN_VER=5.95.0
QT_MIN_VER=5.15.2

DEPENDENCIES="
    build:
        dev-lang/perl:*
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/calendarsupport[>=${PV}]
        kde/eventviews[>=${PV}]
        kde/incidenceeditor[>=${PV}]
        kde/libkdepim[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-calendar:5[>=${PV}]
        kde-frameworks/akonadi-contact:5[>=${PV}]
        kde-frameworks/akonadi-notes:5[>=${PV}]
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kcalutils:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kholidays:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kldap:5[>=${PV}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knewstuff:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kontactinterface:5[>=${PV}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/libX11
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qttools:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        userfeedback? ( kde/kuserfeedback[>=1.2.0] )
    recommendation:
        kde/kde-cli-tools:4 [[
            description = [ Configure date and time formats ]
        ]]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'userfeedback KUserFeedback' )

# 4 of 5 tests need a running X server
RESTRICT="test"

korganizer_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

korganizer_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

