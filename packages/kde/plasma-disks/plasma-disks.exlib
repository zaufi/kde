# Copyright 2020-2022 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

myexparam kf5_min_ver
myexparam qt_min_ver

export_exlib_phases src_test

SUMMARY="Monitors S.M.A.R.T. capable devices for imminent failure."

LICENCES="
    BSD-3 [[ note = [ cmake scripts ] ]]
    || ( GPL-2 GPL )
"
SLOT="0"
MYOPTIONS=""

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build+run:
        kde-frameworks/kauth:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
    run:
        sys-apps/smartmontools
"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    -DWITH_SIMULATION:BOOL=FALSE
)

plasma-disks_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

