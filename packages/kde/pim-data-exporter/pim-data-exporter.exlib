# Copyright 2016 Niels Ole Salscheider <olesalscheider@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]

SUMMARY="Application to import/export data to/from KDE PIM"
DESCRIPTION="
It allows to export and import PIM settings and local mail. You can backup
and restore settings from the following programs:
* kmail
* knotes
* akregator
* kaddressbook
* kalarm
* korganizer
* blogilo
"

LICENCES="FDL-1.2 GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS+="
    userfeedback [[
        description = [ Allows sending anonymized usage information to KDE ]
    ]]
"

KF5_MIN_VER=5.95.0
QT_MIN_VER=5.15.2

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        kde/mailcommon[>=${PV}]
        kde/pimcommon[>=${PV}]
        kde-frameworks/akonadi-notes:5[>=${PV}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcalendarcore:5[>=5.63.0]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcontacts:5[>=5.63.0]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kidentitymanagement:5[>=${PV}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmailtransport:5[>=${PV}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpimtextedit:5[>=${PV}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        userfeedback? ( kde/kuserfeedback[>=1.2.0] )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'userfeedback KUserFeedback' )

# 3 of 4 tests need a running X server
RESTRICT="test"

