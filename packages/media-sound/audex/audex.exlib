# Copyright 2011-2016 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org kde
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE based CDDA extraction tool"
DESCRIPTION="Audex is a audio grabber tool for CD-ROM drives."
HOMEPAGE+=" https://userbase.kde.org/Audex"

LICENCES="GPL-3"
SLOT="0"
MYOPTIONS=""

KF5_MIN_VER="5.15.0"

DEPENDENCIES="
    build+run:
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        kde/libkcddb:5[>=16.11.80]
        media/cdparanoia
        x11-libs/qtbase:5
        x11-libs/qtscript:5
        x11-libs/qtx11extras:5
    suggestion:
        dev-python/eyeD3         [[ description = [ eyeD3 will be prefered over LAME for ID3 tagging ] ]]
        media-libs/faac          [[ description = [ Encode mp4/aac files ] ]]
        media-libs/flac:*        [[ description = [ Encode flac files ] ]]
        media-sound/lame         [[ description = [ Encode mp3 files ] ]]
        media-sound/vorbis-tools [[ description = [ Encode ogg vorbis files ] ]]
"

audex_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

audex_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

