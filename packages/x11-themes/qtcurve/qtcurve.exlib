# Copyright 2014 David Heidelberger <david.heidelberger@ixit.cz>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir=${PN} ] cmake

export_exlib_phases src_prepare

SUMMARY="QtCurve - Qt5/GTK+2 widget styles"
DESCRIPTION="This is a set of widget styles for Qt5 and Gtk2 based apps."

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    gtk2
    kf5 [[ description = [ Builds the Qt5 style with KDE Frameworks 5 support ] ]]
    qt5
    X

    ( gtk2 qt5 ) [[ *requires = X ]]
    kf5 [[ requires = qt5 ]]
"

QT_MIN_VER="5.3.2"

DEPENDENCIES="
    build:
        kde-frameworks/ki18n:5 [[ note = [ translations ] ]]
        gtk2? (
            virtual/pkg-config
        )
        X? (
            virtual/pkg-config
            x11-proto/xcb-proto
        )
    build+run:
        x11-libs/libxcb
        gtk2? (
            dev-libs/glib:2
            x11-libs/cairo
            x11-libs/gdk-pixbuf:2.0
            x11-libs/gtk+:2
            x11-libs/pango
        )
        kf5? (
            kde-frameworks/frameworkintegration:5
            kde-frameworks/karchive:5
            kde-frameworks/kconfig:5
            kde-frameworks/kconfigwidgets:5
            kde-frameworks/kdelibs4support:5
            kde-frameworks/kguiaddons:5
            kde-frameworks/ki18n:5
            kde-frameworks/kiconthemes:5
            kde-frameworks/kio:5
            kde-frameworks/kwidgetsaddons:5
            kde-frameworks/kwindowsystem:5
            kde-frameworks/kxmlgui:5
        )
        qt5? (
            x11-libs/qtbase:5[>=${QT_MIN_VER}]
            x11-libs/qtsvg:5[>=${QT_MIN_VER}]
            x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
        )
        X? (
            x11-libs/libX11
        )
"
CMAKE_SRC_CONFIGURE_PARAMS+=(
    # Used from KF5I18nMacros.cmake
    -DLOCALE_INSTALL_DIR:PATH=/usr/share/locale

    -DENABLE_QT4:BOOL=FALSE
    # Disabled by default upstream
    -DQTC_QT5_ENABLE_QTQUICK2:BOOL=FALSE
)
CMAKE_SRC_CONFIGURE_OPTIONS+=(
    'kf5 QTC_QT5_ENABLE_KDE'
    'X QTC_ENABLE_X11'
)
CMAKE_SRC_CONFIGURE_OPTION_ENABLES+=(
    GTK2
    QT5
)
CMAKE_SRC_CONFIGURE_TESTS+=( '-DENABLE_TEST:BOOL=TRUE -DENABLE_TEST:BOOL=FALSE' )

# Skip failing test
DEFAULT_SRC_TEST_PARAMS+=( -E "test-printf" )

qtcurve_src_prepare() {
    cmake_src_prepare

    # For whatever silly reason upstream actively unsets every variable we
    # could use.
    edo sed \
        -e "s|\${KDE_INSTALL_DATADIR}|/usr/share|" \
        -i qt5/style/CMakeLists.txt
}

