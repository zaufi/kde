# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Lightweight plotting framework"
DESCRIPTION="
KPlotWidget is a QWidget-derived class that provides a virtual base class for
easy data-plotting. The idea behind KPlotWidget is that you only have to
specify information in \"data units\"; i.e., the natural units of the data
being plotted. KPlotWidget automatically converts everything to screen pixel
units."

LICENCES="LGPL-2.1"
MYOPTIONS="
    designer [[ description = [ Install Qt designer plugins ] ]]
"

DEPENDENCIES="
    build+run:
        designer? ( x11-libs/qttools:${major_version}[>=${QT_MIN_VER}] )
"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=(
    'designer DESIGNERPLUGIN'
)

kplotting_src_test() {
    xdummy_start

    default

    xdummy_stop
}

