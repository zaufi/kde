# Copyright 2014-2019 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='ki18n' ]
require test-dbus-daemon gtk-icon-cache

export_exlib_phases src_prepare src_install

SUMMARY="A framework for searching and managing metadata"

LICENCES="GPL-2 LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-db/lmdb
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kfilemetadata:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kidletime:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/solid:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
    run:
        kde-frameworks/kded:${major_version}
"

if [[ $(ever major) == 6 ]] ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qt5compat:6[>=${QT_MIN_VER}]
    "
fi

CMAKE_SRC_CONFIGURE_PARAMS+=( -DBUILD_EXPERIMENTAL:BOOL=FALSE )

baloo_src_prepare() {
    kde_src_prepare

    # These tests need access to the system dbus
    edo sed -e '/fileindexerconfigtest/d' \
            -e '/filtereddiriteratortest/d' \
            -e '/set(fileWatch_SRC filewatchtest.cpp/,+5d' \
            -i autotests/unit/file/CMakeLists.txt
}

baloo_src_install() {
    kde_src_install

    if [[ $(ever major) == 6 ]] ; then
        # Just don't install this for co-installability at the moment
        edo rm "${IMAGE}"/etc/xdg/autostart/baloo_file.desktop \
            "${IMAGE}"/usr/share/dbus-1/interfaces/org.kde.BalooWatcherApplication.xml \
            "${IMAGE}"/usr/share/dbus-1/interfaces/org.kde.baloo.{fileindexer,file.indexer,main,scheduler}.xml \
            "${IMAGE}"/usr/$(exhost --target)/bin/baloo{_file,_file_extractor,ctl,search,show} \
            "${IMAGE}"/usr/$(exhost --target)/lib/systemd/user/kde-baloo.service \
            "${IMAGE}"/usr/$(exhost --target)/libexec/baloo_file{,_extractor}
        edo rmdir "${IMAGE}"/{etc/xdg/autostart,etc/xdg,etc} \
            "${IMAGE}"/usr/share/{dbus-1/interfaces,dbus-1} \
            "${IMAGE}"/usr/$(exhost --target)/{bin,libexec} \
            "${IMAGE}"/usr/$(exhost --target)/lib/{systemd/user,systemd}
    fi
}

