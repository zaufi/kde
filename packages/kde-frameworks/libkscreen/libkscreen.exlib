# Copyright 2013, 2015-2022 Heiko Becker <heirecka@exherbo.org>
# Copyright 2014 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde

myexparam kf5_min_ver
myexparam qt_min_ver

SUMMARY="KDE's screen management library"

LICENCES="GPL-2"
SLOT="5"
MYOPTIONS+="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

exparam -v KF5_MIN_VER kf5_min_ver
exparam -v QT_MIN_VER qt_min_ver

DEPENDENCIES="
    build:
        kde/plasma-wayland-protocols[>=1.6.0]
        virtual/pkg-config
        x11-libs/qtwayland:5 [[ note = qtwaylandscanner ]]
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        kde-frameworks/kwayland:5[>=${KF5_MIN_VER}]
        sys-libs/wayland[>=1.15]
        x11-libs/libxcb   [[ note = [ Could be optional with 5.6.x ] ]]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}]
"

if ever at_least 5.25.90 ; then
    DEPENDENCIES+="
        build+run:
            kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
    "
fi

# Tests need a running X server (1.0.2)
RESTRICT=test

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

