# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Set of item models extending the Qt model-view framework"
DESCRIPTION="
KItemModels provides the following models:
* KBreadcrumbSelectionModel - Selects the parents of selected items to create
  breadcrumbs
* KCheckableProxyModel - Adds a checkable capability to a source model
* KDescendantsProxyModel - Proxy Model for restructuring a Tree into a list
* KLinkItemSelectionModel - Share a selection in multiple views which do not
  have the same source model
* KModelIndexProxyMapper - Mapping of indexes and selections through proxy
  models
* KRecursiveFilterProxyModel - Recursive filtering of models
* KSelectionProxyModel - A Proxy Model which presents a subset of its source
  model to observers"

LICENCES="LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
"

if [[ ${major_version} == 5 ]] ; then
    DEPENDENCIES+="
        test:
            x11-libs/qtscript:5[>=${QT_MIN_VER}]
    "
fi

kitemmodels_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

