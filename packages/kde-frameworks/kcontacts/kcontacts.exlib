# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ]

export_exlib_phases src_test

SUMMARY="Provides an API for address book data"
DESCRIPTION="This can be used by all KDE application using data of this type, e.g. KAddressBook,
KMail, KOrganizer, KPilot etc. It is meant as replacement for libkab (in kdebase/kab).

Features:

- Value based interface, addressbook entry data is implicitly shared.
- Address book entries are identified by a unique id.
- vCard backend (RFC 2425 / RFC 2426).
- Locking mechanism to support concurrent access to the address book by
  multiple processes.
- Notification on change of addressbook by other process.
- Dialog for selecting address book entries, supports mouse and keyboard
  selection, supports automatic name completion.
- GUI client for viewing, modifying address book data. This is aimed at
  developers not end users.
- Tool for converting data, written with libkab, to libkabc format.
- Multiple backends (resources) for storing entries e.g. LDAP"

LICENCES="LGPL-2.1"

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        kde-frameworks/kcodecs:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
"

if [[ ${major_version} == 6 ]] ; then
    DEPENDENCIES+="
        build+run:
            x11-libs/qt5compat:6
    "
fi

DEFAULT_SRC_TEST_PARAMS+=(
    # Skip failing test (https://bugs.kde.org/show_bug.cgi?id=397185)
    ARGS=" -E kcontacts-addresstest"
)

kcontacts_src_test() {
    xdummy_start

    default

    xdummy_stop
}

