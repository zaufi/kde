# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks [ docs=false ] kde [ kf_major_version=${major_version} translations='qt' ]

export_exlib_phases src_install

SUMMARY="Central daemon of KDE work spaces"

LICENCES="LGPL-2.1"
MYOPTIONS="doc"

DEPENDENCIES="
    build:
        doc? ( kde-frameworks/kdoctools:${major_version}[>=${KF5_MIN_VER}] )
    build+run:
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=( 'doc KF5DocTools' )

kded_src_install() {
    kde_src_install

    if [[ ${major_version} == 6 ]] ; then
        # Just don't install this for co-installability at the moment, even
        # though it's a bit ridiculouse beause this package provdes no
        # library, but at least we'll compile test it.
        edo rm "${IMAGE}"/usr/share/applications/org.kde.kded5.desktop \
            "${IMAGE}"/usr/share/dbus-1/{interfaces/org.kde.kded5.xml,services/org.kde.kded5.service} \
            "${IMAGE}"/usr/share/man/man8/kded5.8 \
            "${IMAGE}"/usr/$(exhost --target)/bin/kded5 \
            "${IMAGE}"/usr/$(exhost --target)/lib/systemd/user/plasma-kded.service
        edo rmdir "${IMAGE}"/usr/share/applications \
            "${IMAGE}"/usr/share/{dbus-1/interfaces,dbus-1/services,dbus-1} \
            "${IMAGE}"/usr/share/{man/man8,man} \
            "${IMAGE}"/usr/$(exhost --target)/bin \
            "${IMAGE}"/usr/$(exhost --target)/lib/{systemd/user,systemd}
    fi
}

