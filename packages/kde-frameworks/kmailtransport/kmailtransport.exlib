# Copyright 2015 Bernd Steinhauser <berniyh@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test

SUMMARY="Framework for managing mail transports"

LICENCES="LGPL-2.1"
SLOT="5"
MYOPTIONS="
    doc [[ description = [ Build API docs in Qt's help format (.qch) ] ]]
"

KF5_MIN_VER=5.95.0
QT_MIN_VER=5.15.2

DEPENDENCIES="
    build:
        doc? (
            app-doc/doxygen
            x11-libs/qttools:5   [[ note = qhelpgenerator ]]
        )
    build+run:
        app-pim/libkgapi[>=${PV}]
        kde/ksmtp[>=${PV}]
        kde-frameworks/akonadi-mime:5[>=${PV}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kmime:5[>=${PV}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        server-pim/akonadi:5[>=${PV}]
        sys-auth/qtkeychain[providers:qt5]
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
    test:
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
"

# Wants to start an akonadi and MySQL server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_BUILDS+=( 'doc QCH' )

kmailtransport_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

