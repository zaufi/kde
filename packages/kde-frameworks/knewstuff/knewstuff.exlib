# Copyright 2014-2020 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

myexparam kf_major_version=5
exparam -v major_version kf_major_version

require kde-frameworks kde [ kf_major_version=${major_version} translations='ki18n' ]
require xdummy [ phase=test ] test-dbus-daemon

export_exlib_phases src_test src_install

SUMMARY="Framework for downloading and sharing additional application data"
DESCRIPTION="
The KNewStuff library implements collaborative data sharing for
applications. It uses libattica to support the Open Collaboration Services
specification."

LICENCES="LGPL-2.1"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        kde-frameworks/attica:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/karchive:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kio:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kpackage:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:${major_version}[>=${KF5_MIN_VER}]
        kde-frameworks/syndication:${major_version}[>=${KF5_MIN_VER}]
        x11-libs/qtdeclarative:${major_version}[>=${QT_MIN_VER}]
"

if [[ ${major_version} == 6 ]] ; then
    DEPENDENCIES+="
        run:
            kde-frameworks/kirigami:6[>=2.12.0]
    "
else
    DEPENDENCIES+="
        run:
            kde-frameworks/kirigami:2[>=2.12.0]
    "
fi

knewstuff_src_test() {
    xdummy_start

    test-dbus-daemon_run-tests

    xdummy_stop
}

knewstuff_src_install() {
    kde_src_install

    if [[ ${major_version} == 6 ]] ; then
        # Just don't install this for co-installability at the moment
        edo rm "${IMAGE}"/usr/$(exhost --target)/bin/knewstuff-dialog
        edo rmdir "${IMAGE}"/usr/$(exhost --target)/bin
    fi
}

