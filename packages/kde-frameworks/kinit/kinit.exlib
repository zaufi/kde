# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-frameworks [ docs=false ] kde [ translations='ki18n' ]

export_exlib_phases pkg_postinst

SUMMARY="Helper library to speed up start of applications on KDE workspaces"
DESCRIPTION="
kdeinit is a process launcher somewhat similar to the famous init used for
booting UNIX.
It launches processes by forking and then loading a dynamic library which
should contain a 'kdemain(...)' function.
Using kdeinit to launch KDE applications makes starting a typical KDE
applications 2.5 times faster (100ms instead of 250ms on a P-III 500) It
reduces memory consumption by approx. 350Kb per application."

LICENCES="BSD-3 LGPL-2.1"
MYOPTIONS="
    doc
    X [[ presumed = true ]]
"

DEPENDENCIES="
    build:
        doc? ( kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}] )
    build+run:
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        sys-libs/libcap
        X? (
            x11-libs/libX11
            x11-libs/libxcb
        )
"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'doc KF5DocTools'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS+=(
    'X X11'
)

kinit_pkg_postinst() {
    # allow start_kdeinit to use capabilities instead of SUID
    # doesn't carry over from src_install so we need to set this manually
    edo setcap cap_sys_resource+ep /usr/$(exhost --target)/libexec/kf5/start_kdeinit
}

