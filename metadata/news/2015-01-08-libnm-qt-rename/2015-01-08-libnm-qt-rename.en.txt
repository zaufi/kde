Title: kde-frameworks/libnm-qt has been renamed to networkmanager-qt
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2015-01-08
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde-frameworks/libnm-qt

Please install kde-frameworks/networkmanager-qt and *afterwards* uninstall
kde-frameworks/libnm-qt.

1. Take note of any packages depending on libnm-qt:
cave resolve \!kde-frameworks/libnm-qt

2. Install kde-frameworks/networkmanager-qt:
cave resolve networkmanager-qt -x

3. Re-install the packages from step 1.

4. Uninstall kde-frameworks/libnm-qt
cave resolve \!kde-frameworks/libnm-qt -x

Do it in *this* order or you'll potentially *break* your system.
